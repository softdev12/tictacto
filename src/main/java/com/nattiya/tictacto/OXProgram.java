/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nattiya.tictacto;

import java.util.Scanner;

/**
 *
 * @author DELL
 */
public class OXProgram {

    static char table[][] = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}
    };

    static int row, col;
    static Scanner kb = new Scanner(System.in);

    static char player = 'X';
    static boolean isFinish = false;
    static char winner = '-';
    static int turn = 0;

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            swichPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println();
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            System.out.println("please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table[row][col] == '-') {
                table[row][col] = player;
                turn++;
                break;
            }
            System.out.println("Error: table at row and col is not empty!!");
        }
        showTable();
        while (true) {
            System.out.println("please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Error: table at row and col is not empty!!");
        }
    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkX() {

        if (table[0][2] == player
                && table[1][1] == player
                && table[2][0] == player) {
            isFinish = true;
            winner = player;
        } else if (table[0][0] == player
                && table[1][1] == player
                && table[2][2] == player) {
            isFinish = true;
            winner = player;
        }
    }

    static void checkDraw() {
        if (turn == 9) {
            isFinish = true;
        }
    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkDraw();

    }

    static void swichPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        showTable();
        if (winner == '-') {
            System.out.println("Draw!!");
        } else {
            System.out.println(winner + " Win!!");
        }
    }

    static void showBye() {
        System.out.println("Bye bye ...");
    }

}
